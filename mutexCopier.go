package main

import "sync"

type MutexCopier struct {
	sync.Mutex
	sync.WaitGroup
	dp      DataProvider
	n, r, s int
}

func (cp *MutexCopier) Setup(dp DataProvider, n, r, s int) {
	cp.n = n
	cp.r = r
	cp.s = s
	cp.dp = dp
}

func (cp *MutexCopier) Copy(l *[]string) {
	cp.Add(1)
	go cp.copyInternal(l, cp.r)

	cp.Wait()
}

func (cp *MutexCopier) copyInternal(l *[]string, r int) {
	defer cp.Done()
	if r > 0 {
		for s := 0; s < cp.s; s++ {
			cp.Add(1)
			go cp.copyInternal(l, r-1)
		}
	}

	for n := 0; n < cp.n; n++ {
		dat := cp.dp.ProvideString()

		// faster to do this individually instead of pre-appending them
		cp.Lock()
		*l = append(*l, dat...)
		cp.Unlock()
	}
}
