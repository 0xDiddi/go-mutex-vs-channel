package main

import (
	"fmt"
	"testing"
)

func init() {
	dp = &RandomDataProvider{}
	cp1 = &MutexCopier{}
	cp2 = &ChannelCopier{}

}

var (
	// numbersLarge   = []int{4, 32, 128, 512, 1024, 8192, 32768}
	numbersSmall   = []int{4, 32, 128, 256}
	numbersDpl     = []int{16}
	numbersCpn     = []int{1, 4}
	numbersRecurse = []int{0, 1}
	numbersCps     = []int{1, 2}
)

func Benchmark(b *testing.B) {
	for _, dpn := range numbersSmall {
		for _, dpl := range numbersDpl {
			for _, cpn := range numbersCpn {
				for _, cpr := range numbersRecurse {
					for _, cps := range numbersCps {
						name := fmt.Sprintf("%d-%d %d-%d-%d\n", dpn, dpl, cpn, cpr, cps)

						b.Run(name, internal(dpn, dpl, cpn, cpr, cps))
					}
				}
			}
		}
	}
}

func internal(dpn, dpl, cpn, cpr, cps int) func(b *testing.B) {
	return func(b *testing.B) {
		dp.Setup(dpn, dpl)

		cp1.Setup(dp, cpn, cpr, cps)
		cp2.Setup(dp, cpn, cpr, cps)

		b.Run("mutex", runMutex)
		b.Run("channel", runChannel)
	}
}

func runMutex(b *testing.B) {
	for i := 0; i < b.N; i++ {
		l := make([]string, 0)
		cp1.Copy(&l)
	}
}

func runChannel(b *testing.B) {
	for i := 0; i < b.N; i++ {
		l := make([]string, 0)
		cp2.Copy(&l)
	}
}
