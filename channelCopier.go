package main

import "sync"

type ChannelCopier struct {
	sync.WaitGroup
	dp      DataProvider
	n, r, s int
}

func (cp *ChannelCopier) Setup(dp DataProvider, n, r, s int) {
	cp.n = n
	cp.r = r
	cp.s = s
	cp.dp = dp
}

func (cp *ChannelCopier) Copy(l *[]string) {
	c := make(chan []string, 32)

	cp.Add(1)
	go cp.copyInternal(c, cp.r)

	go func() {
		cp.Wait()
		close(c)
	}()

	for d := range c {
		*l = append(*l, d...)
	}
}

func (cp *ChannelCopier) copyInternal(c chan []string, r int) {
	defer cp.Done()
	if r > 0 {
		for s := 0; s < cp.s; s++ {
			cp.Add(1)
			go cp.copyInternal(c, r-1)
		}
	}

	for n := 0; n < cp.n; n++ {
		dat := cp.dp.ProvideString()

		// faster to do this individually instead of pre-appending them
		c <- dat
	}
}
