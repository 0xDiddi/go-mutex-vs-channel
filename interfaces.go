package main

type DataProvider interface {
	Setup(n, l int)
	ProvideString() []string
}

type Copier interface {
	// for r recursion levels, do n requests each, and start s subroutines
	Setup(dp DataProvider, n, r, s int)
	Copy(l *[]string)
}
